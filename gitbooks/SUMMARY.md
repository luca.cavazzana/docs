# Table of contents

* [Getting Started](README.md)
* [Installing](installing/README.md)
  * [Prerequisites](installing/prerequisites.md)
  * [Installation](installing/installation.md)

## Packs

* [🐧 Linux](packs/linux.md)
* [☕ JVM](packs/jvm.md)
