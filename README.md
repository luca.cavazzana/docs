# Documentation

## Desired

* Markdown support, which allows an easy way to automate some activities (opack doc, cli commands, ...)
* Multi-version support
* Auth (for some areas at least)

## Feature matrix

|  | Built on|  Write in | Auth | Search
| :--- | :---: | :---: | :---: | :---:
| MkDocs| | Markdown | ❌ | Yes
| Docusaurus | Node.js | Markdown | ❌ | Yes but depends on Algolia
