---
sidebar_position: 4
---

# Akamas - Getting Started

Akamas is an AI-driven full-stack solution for continuous performance optimization. It continuously optimizes IT stack configurations to deliver unprecedented application performance, deployment agility and cost savings.

## Pillars

Akamas is based on four technological pillars.

### Machine Learning (ML)

Akamas uses ML to solve intractable optimization problems. By intelligently shrinking the target configuration space, it delivers massive performance and cost improvements in hours instead of weeks.

### Automation

Akamas radically outperforms trial-and-error manual optimization by using automation to iteratively design performance experiments, analyze outcomes and deploy settings.

### Goal-driven

Akamas discovers the best configuration tradeoffs for your performance goals. Availability. Throughput. Response time. Cost. You set the goal, Akamas figures out how to achieve it.

### Full-Stack

Akamas is a smart, technology-agnostic optimization platform that understands the interdependencies between operating system, middleware and application configurations.

## Fundamentals

Akamas is a platform designed to be applied in any context. Therefore, it can be used in very different scenarios such as full-stack IT optimization or hyperparameter tuning of ML models. 

The following sections introduce the core concepts used by Akamas. These concepts can be used to model the environment and the scope of the optimization in order to drive Akamas exploration towards specific business goals. This document is an introduction to Akamas. Therefore, you are not required to have prior experience using Akamas. 

Most of the examples in the following sections relate to optimization activities of IT stacks. Therefore, some knowledge of this domain or some experience in setting configuration parameters or running a performance test is beneficial. 
