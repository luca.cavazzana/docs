---
sidebar_position: 1
---

# Description

The _MongoDB_ optimization pack helps you optimize instances of MongoDB to reach the desired performance goal. The optimization pack provides parameters and metrics specific to MongoDB that can be leveraged to reach, among others, two main goals:

* Throughput optimization - increasing the capacity of a MongoDB deployment to serve clients 
* Cost optimization - decreasing the size of a MongoDB deployment while guaranteeing the same service level

To reach such goals the pack focuses mostly on the parameters managing the cache, being one of the elements that impact performances the most; in particular, the optimization pack provides parameters to control the lifecycle and the size of the MongoDB’s cache thus significantly impacting performance.

Even though it is possible to evaluate performance improvements of MongoDB by looking at the business application that uses it as its database, looking at the end to end throughput or response time or using a performance test like YCSB, the optimization pack provides internal MongoDB metrics that can shed a light too on how MongoDB is performing, in particular in terms of throughput, for example:

* The number of documents inserted in the database per second
* The number of active connections

All of the most used versions o MongoDB are supported, for more info see the Component Type section.

## Installing

Here’s the command to install the MongoDB optimization-pack using the Akamas CLI:

```bash
akamas install optimization-pack MongoDB
```

For more information on the process of installing or upgrading an optimization pack refer to Managing Optimization Packs 

...
