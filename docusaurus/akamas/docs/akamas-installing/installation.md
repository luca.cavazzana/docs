# Installation

## Get Docker artifacts

Akamas is deployed as a set of containerized services running on Docker and managed via Docker Compose.
Since you have access to the public internet you can get the latest version of the Akamas Docker Compose file with the specs of all the images required by Docker.
We suggest creating a folder named akamas in the home directory of your user, then simply run the following command to get the most updated file:

```bash
cd ~
mkdir akamas
cd akamas
curl -O https://s3.us-east-2.amazonaws.com/akamas/compose/$(curl https://s3.us-east-2.amazonaws.com/akamas/compose/stable.txt)/docker-compose.yml
```

## Configure environment variables

You need to set the following environment variables to configure Akamas properly:

**AKAMAS_CUSTOMER**: the customer name. The name has to match the one linked to the Akamas license.

**AKAMAS_BASE_URL**: it is the endpoint of the Akamas APIs that will be used to interact with the CLI. Since you are installing Akamas as an all-in-one instance you can set it to localhost port 8000.

:::caution
You’d better save all exported variables in your ~/.bashrc file for convenience
:::

Environment variables creation is performed by the snippet below:

```bash
# add double quotes ("xx xx") if the name contains white spaces
export AKAMAS_CUSTOMER=<your name or your organization name>
export AKAMAS_BASE_URL=http://<akamas server dns address>:8000
In order to login into AWS ECR and pull the updated Akamas containers images you also need to set the AWS authentication variables accordingly to the values shared by your Professional Services contact person.
To set the variables run the following command:
```

```bash
export AWS_ACCESS_KEY_ID=<your access key id>
export AWS_SECRET_ACCESS_KEY=<your secret access key>
export AWS_DEFAULT_REGION=us-east-2
After that, log in using the AWS CLI dedicated command: 
```

```bash
aws ecr get-login-password --region us-east-2 | docker login -u AWS --password-stdin https://485790562880.dkr.ecr.us-east-2.amazonaws.com
```

...
