# MongoDB 5

:::tip COOL!
This page was automatically generate by the scripts already in the deploy repo
:::

## Metrics

<details><summary>metrics/metrics.yaml</summary><p>

|name|unit|description|
|---|---|---|
|mongodb_opcounters_insert|operations/s|The number of insert operations received per second.|
|mongodb_opcounters_query|operations/s|The number of queries received per second.|
|mongodb_opcounters_update|operations/s|The number of update operations received per second.|
|mongodb_opcounters_delete|operations/s|The number of delete operations received per second.|
|mongodb_opcounters_getmore|operations/s|The number of getMore operations received per second. This counter can be high even if the query count is low. Secondary nodes send getMore operations as part of the replication process.|
|mongodb_opcounters_command|operations/s|The number of command operations received per second. It counts all commands except the write commands (insert, update, and delete).|
|mongodb_documents_deleted|documents/s|The number of documents deleted per second.|
|mongodb_documents_inserted|documents/s|The number of documents inserted per second.|
|mongodb_documents_returned|documents/s|The number of documents returned per second.|
|mongodb_documents_updated|documents/s|The number of documents updated per second.|
|mongodb_wt_concurrentTransactions_read_out|tickets|Number of read tickets in use|
|mongodb_wt_concurrentTransactions_write_out|tickets|Number of write tickets in use|
|mongodb_wt_concurrentTransactions_read_available|tickets|Number of read tickets remaining. When it reaches 0, read requests will be queued. The maximum number of read operations is controlled with wiredTigerConcurrentReadTransactions (or by adding more shards)|
|mongodb_wt_concurrentTransactions_write_available|tickets|Number of write tickets remaining. When it reaches 0, write requests will be queued. The maximum number of read operations is controlled with wiredTigerConcurrentWriteTransactions (or by adding more shards)|
|mongodb_wt_transaction_most_recent_time|milliseconds|Amount of time, in milliseconds, to create the most recent checkpoint. An increase in this value under stead write load may indicate saturation on the I/O subsystem.|
|mongodb_metrics_cursor_open_total|cursors|The number of cursors that MongoDB is maintaining for clients. Because MongoDB exhausts unused cursors, typically this value small or zero. However, if there is a queue, stale tailable cursors, or a large number of operations this value may rise.|
|mongodb_metrics_cursor_timedOut|cursors|The total number of cursors that have timed out since the server process started. If this number is large or growing at a regular rate, this may indicate an application error.|
|mongodb_connections_current|connections|The number of incoming connections from clients to the database server, including connections from other servers such as replica set members or mongos instances.|
|mongodb_connections_available|connections|The number of unused incoming connections available.|
|mongodb_connections_active|connections|The number of active client connections to the server. Active client connections refers to client connections that currently have operations in progress.|
|mongodb_mem_resident|megabytes|Roughly equivalent to the amount of RAM, in mebibyte (MiB), currently used by the database process. During normal use, this value tends to grow. In dedicated database servers, this number tends to approach the total amount of system memory.|
|mongodb_mem_virtual|megabytes|Displays the quantity, in mebibyte (MiB), of virtual memory used by the mongod process.|
|mongodb_extra_info_page_faults|faults/s|The number of page faults per second. The counter may increase dramatically during moments of poor performance and may correlate with limited memory environments and larger data sets.|
|mongodb_wt_cache_bytes_currently_in_the_cache|bytes|Size in byte of the data currently in WiredTiger internal cache.|
|mongodb_wt_cache_maximum_bytes_configured|bytes|Maximum WiredTiger internal cache size.|
|mongodb_wt_cache_unmodified_pages_evicted|pages/s|Number of unmodified pages evicted from the cache per second|
|mongodb_wt_cache_modified_pages_evicted|pages/s|Number of modified pages evicted from the cache per second|
|mongodb_wt_cache_tracked_dirty_bytes_in_the_cache|bytes|Size in bytes of the dirty data in the cache.|
|mongodb_wt_cache_pages_read_into_cache|pages/s|Number of pages read into the cache per second. Together with the write metric, it can provide an overview of the I/O activity.|
|mongodb_wt_cache_pages_written_from_cache|pages|Number of pages written from the cache. Together with the read metric, it can provide an overview of the I/O activity.|
|mongodb_globalLock_currentQueue_total|operations|The total number of operations queued waiting for the lock (sum of readers and writers). A consistently small queue, particularly of shorter operations, should cause no concern.|
|mongodb_globalLock_currentQueue_readers|operations|The number of operations that are currently queued and waiting for the read lock. A consistently small read-queue, particularly of shorter operations, should cause no concern.|
|mongodb_globalLock_currentQueue_writers|operations|The number of operations that are currently queued and waiting for the write lock. A consistently small write-queue, particularly of shorter operations, is no cause for concern.|
|mongodb_globalLock_activeClients_total|connections|The total number of internal client connections to the database including system threads as well as queued readers and writers. This metric will be higher than the total of readers and writers to the inclusion of system threads.|
|mongodb_globalLock_activeClients_readers|connections|The number of the active client connections performing read operations.|
|mongodb_globalLock_activeClients_writers|connections|The number of the active client connections performing write operations.|
</p></details>

## Parameters
<details><summary>parameters/parameters.yaml</summary><p>

|name|type|unit|default|domain|restart|description|
|---|---|---|---|---|---|---|
|mongodb_cursorTimeoutMillis|integer|milliseconds|`600000`|`0` → `1200000`|no|Sets the expiration threshold in milliseconds for idle cursors before MongoDB removes them.|
|mongodb_notablescan|categorical| |`false`|`true`, `false`|no|Return an error when executing queries that don't use indices.|
|mongodb_ttlMonitorEnabled|categorical| |`true`|`true`, `false`|no|Disable the TTL monitor works, preventing TTL documents removal.|
|mongodb_disableJavaScriptJIT|categorical| |`true`|`true`, `false`|no|The MongoDB JavaScript engine uses SpiderMonkey, which implements Just-in-Time (JIT) compilation for improved performance when running scripts.|
|mongodb_maxIndexBuildMemoryUsageMegabytes|integer|megabytes|`200`|`50` → `2000`|no|Limits the amount of memory that simultaneous index builds on one collection may consume for the duration of the builds. The memory consumed by an index build is separate from the WiredTiger cache memory.|
|mongodb_tcmallocReleaseRate|real| |`1.0`|`0.0` → `10.0`|no|Rate at which we release unused memory to the system, via madvise(MADV_DONTNEED), on systems that support it. Zero means we never release memory back to the system.|
|mongodb_journalCommitInterval|integer|milliseconds|`100`|`1` → `500`|no|The number of milliseconds (ms) between journal commits.|
|mongodb_syncdelay|integer|seconds|`60`|`0` → `300`|no|The interval in seconds between fsync operations where mongod flushes its working memory to disk. By default, mongod flushes memory to disk every 60 seconds. In almost every situation you should not set this value and use the default setting.|
|mongodb_internalQueryEnableSlotBasedExecutionEngine|categorical| |`true`|`true`, `false`|no|Use enhanced query execution when possible.|
|mongodb_planCacheSize|integer|percent|`5`|`0` → `99`|no|The size of the plan cache for the enhanced query execution engine.|
|mongodb_wterc_cache_overhead|integer|percent|`8`|`0` → `30`|no|Amount of additional heap to allocate expressed as percentage of the heap.|
|mongodb_wterc_cache_size|integer|megabytes|`100`|`1` → `10000000`|no|Maximum heap memory to allocate for the cache. A database should configure either cache_size or shared_cache but not both. This should correspond to the value used in MongoDB, so set it to a minimum of 256 MB or to 50% of (RAM - 1 GB)|
|mongodb_wterc_checkpoint_log_size|integer|bytes|`0`|`0` → `2000000000`|no|Minimum number of bites to be written between checkpoints. Setting the value to 0 configures periodic checkpoints.|
|mongodb_wterc_checkpoint_wait|integer|seconds|`0`|`0` → `100000`|no|Seconds to wait between periodic checkpoints.|
|mongodb_wterc_eviction_threads_max|integer| |`8`|`1` → `20`|no|Maximum number of threads WiredTiger will start to help evict pages from cache.|
|mongodb_wterc_eviction_threads_min|integer| |`1`|`1` → `20`|no|Minimum number of threads WiredTiger will start to help evict pages from cache.|
|mongodb_wterc_eviction_checkpoint_target|integer|percent|`1`|`0` → `99`|no|Perform eviction at the beginning of checkpoints to bring the dirty content in cache to this level, expressed as a percentage of the total cache size. Ignored if set to zero or in_memory is true.|
|mongodb_wterc_eviction_dirty_target|integer|percent|`5`|`1` → `99`|no|Perform eviction in worker threads when the cache contains at least this much dirty content, expressed as a percentage of the total cache size.|
|mongodb_wterc_eviction_dirty_trigger|integer|percent|`20`|`1` → `99`|no|Trigger application threads to perform eviction when the cache contains at least this much dirty content, expressed as a percentage of the total cache size. This setting only alters behavior if it is lower than eviction_trigger.|
|mongodb_wterc_eviction_target|integer|percent|`80`|`10` → `99`|no|Perform eviction in worker threads when the cache contains at least this much content, expressed as a percentage of the total cache size. Must be less than eviction_trigger.|
|mongodb_wterc_eviction_trigger|integer|percent|`95`|`10` → `99`|no|Trigger application threads to perform eviction when the cache contains at least this much content, expressed as a percentage of the total cache size.|
|mongodb_wterc_file_manager_close_handle_minimum|integer| |`250`|`0` → `1000`|no|Number of handles open before the file manager will look for handles to close.|
|mongodb_wterc_file_manager_close_idle_time|integer|seconds|`30`|`0` → `100000`|no|Amount of time in seconds a file handle needs to be idle before attempting to close it. A setting of 0 means that idle handles are not closed.|
|mongodb_wterc_file_manager_close_scan_interval|integer|seconds|`10`|`1` → `100000`|no|Interval in seconds at which to check for files that are inactive and close them.|
|mongodb_wterc_log_archive|categorical| |`true`|`true`, `false`|no|Automatically archive unneeded log files.|
|mongodb_wterc_log_prealloc|categorical| |`true`|`true`, `false`|no|Pre-allocate log files.|
|mongodb_wterc_log_zero_fill|categorical| |`false`|`true`, `false`|no|Manually write zeroes into log files.|
|mongodb_wterc_lsm_manager_merge|categorical| |`true`|`true`, `false`|no|Merge LSM chunks where possible.|
|mongodb_wterc_lsm_manager_worker_threads_max|integer| |`4`|`3` → `20`|no|Configure a set of threads to manage merging LSM trees in the database.|
|mongodb_wterc_concurrent_read_transactions|integer|transactions|`128`|`1` → `8192`|no|Configure the number of concurrent read transactions allowed into the WiredTiger storage engine.|
|mongodb_wterc_concurrent_write_transactions|integer|transactions|`128`|`1` → `8192`|no|Configure the number of concurrent write transactions allowed into the WiredTiger storage engine.|
|mongodb_wterc_cursor_cache_size|integer|cursors|`-100`|`-100000` → `100000`|no|The absolute value of this parameter sets the maximum number of cursors cached at levels above the WiredTiger storage engine. Zero or negative values also enable the caching at WiredTiger level.|
|mongodb_wterc_session_close_idle_time|integer|seconds|`300`|`0` → `3600`|no|Idle time in seconds before WiredTiger sessions are removed from the session cache.|
</p></details>

