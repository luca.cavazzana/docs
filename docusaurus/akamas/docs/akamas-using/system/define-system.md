---
sidebar_position: 1
---

# Define a System

Systems are defined using a YAML manifest with a structure like the one below:

```yaml
# General section
name: Analytical functions
description: A collection of analytical functions
```

## General section

In this section general and simple properties of a component are defined.

| Field | Type | Values restrictions | Is required | Default value | Description
|---|---|---|---|---|---
| name | STRING | TRUE | | | The name of the system
| description | STRING | TRUE | | | A description to characterize the system

## Manifests samples

A system for Cassandra

```yaml
name: system1
description: my system with 3 nodes of cassandra
```
